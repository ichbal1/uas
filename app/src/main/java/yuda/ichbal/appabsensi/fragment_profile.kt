package yuda.ichbal.appabsensi

import android.app.AlertDialog
import android.content.*
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.ContextMenu
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import kotlinx.android.synthetic.main.editprofile.*
import kotlinx.android.synthetic.main.editprofile.view.*


class fragment_profile : Fragment(), View.OnClickListener {

    lateinit var  v : View
    lateinit var  thisparent : MainActivity
    lateinit var db : SQLiteDatabase
    val nm : String =""
    lateinit var  dialog : AlertDialog.Builder
    lateinit var pref : SharedPreferences
    //lateinit var fr : FragmentTransaction
    //lateinit var login : activityLogin


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.editprofile,container,false)
        thisparent =  activity as MainActivity
        db = thisparent.getdataobjec()
        dialog = AlertDialog.Builder(thisparent)
        v.button2.setOnClickListener(this)

        return v
    }



    override fun onClick(v: View?) {
        when(v?.id){
            R.id.button2->{
                var user = ""
                pref = activity!!.getSharedPreferences("ubah", Context.MODE_PRIVATE)
                user = pref.getString("nam","").toString()
                var sql = "select password from user where nama ='$user'"
                val c : Cursor = db.rawQuery(sql,null)
                c.moveToFirst()
                var aa = c.getString(0)
                if (edtpass.text.toString() == aa  ){
                    //Toast.makeText(activity!!.baseContext,"benar",Toast.LENGTH_SHORT).show()

                    if(pasru.text.toString() == null && konfirpass.text.toString()== null){
                        Toast.makeText(activity!!.baseContext,"anda belum mengisi password baru anda",Toast.LENGTH_SHORT).show()
                    }
                    if (pasru.text.toString() == konfirpass.text.toString()){
                        dialog.setTitle("NOTIFIKASI").setIcon(android.R.drawable.ic_dialog_info)
                            .setMessage("YAKIN PASWORMU DI GANTI!!")
                            .setPositiveButton("Yakin", action)
                            .setNegativeButton("Gak", gak )
                        dialog.show()

                    }else{
                        konfirpass.setText("")
                        Toast.makeText(activity!!.baseContext,"Konfirmasi yang anda masukan tidak sama dengan password baru anda",Toast.LENGTH_SHORT).show()
                    }
                }else{
                    Toast.makeText(activity!!.baseContext,"Pasword anda salah",Toast.LENGTH_SHORT).show()
                    edtpass.setText("")
                }


            }
        }
    }

    fun update(password :String){
        var user =""
        pref = activity!!.getSharedPreferences("ubah", Context.MODE_PRIVATE)
        user = pref.getString("nam","").toString()
        val cv : ContentValues = ContentValues()
        cv.put("password", password)
        db.update("user",cv,"nama ='$user'",null)
    }

    val action = DialogInterface.OnClickListener { dialog, which ->
        update(konfirpass.text.toString())
        pasru.setText("")
        konfirpass.setText("")
        edtpass.setText("")

        val inten = Intent(activity!!, login::class.java)
        startActivity(inten)
    }
    val gak = DialogInterface.OnClickListener { dialog, which ->
        Toast.makeText(activity!!.baseContext,"Pin tidak jadi di ganti",Toast.LENGTH_LONG).show()
        pasru.setText("")
        konfirpass.setText("")
        edtpass.setText("")
    }


}
