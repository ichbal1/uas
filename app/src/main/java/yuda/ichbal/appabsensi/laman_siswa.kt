package yuda.ichbal.appabsensi

import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_siswa.*

class laman_siswa : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var ft : FragmentTransaction
    lateinit var db : SQLiteDatabase


    //main
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)

        db = OpenHelper(this).writableDatabase
    }


    //sabung database
    fun  getdataobjec(): SQLiteDatabase {
        return db
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){

            R.id.itemabout-> layout.visibility =  View.GONE
        }
        return true
    }
}