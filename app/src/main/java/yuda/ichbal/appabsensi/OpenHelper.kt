package yuda.ichbal.appabsensi

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class OpenHelper(context: Context):SQLiteOpenHelper(context,Db_name,null,Db_ver) {

    companion object{
        val Db_name = "mahasiswa"
        val Db_ver = 1
    }


    override fun onCreate(db: SQLiteDatabase?) {


        val tUser = "create table user(id_user Int primary key, nama text not null , password text not null, id_level text not null)"
        val  tlevel = "create table level(id_level integer primary key autoincrement, nama_level text not null)"
        val inslevel = "insert into level(nama_level) values ('ADMIN'),('GURU'),('ORANG TUA'),('SISWA')"
        val insuser = "insert into user(id_user,nama,password,id_level) values ('1234','admin','admin','1')"

        db?.execSQL(tlevel)
        db?.execSQL(tUser)

        db?.execSQL(inslevel)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


}

